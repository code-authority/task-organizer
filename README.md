# task-organizer

## Project Description

### Tech Stack

- Angular 11
- Angular Material

### MVP1 Features

- Sign-up/Login using email
- Create Project
- Create Task
- Assign Task to Project
- Set/Change Task Priority
- Set/Change Task Due Date
- See Daily/Weekly/Monthly view of Tasks
- Search Task

### MVP2 Features

- Add/Remove recurring Tasks
- Color code projects
- Icons for tasks
- Create Family/Team, assign task to Family/Team
- Set/Receive Reminders

### MVP3 Features

- SSO Login via Google/Facebook/GitHub etc.
- Personalize, add own photos, screen styles etc.
- Progressive Web App
- Productivity Score

## How to setup and install

### Angular UI

1. Clone the repository.
2. Run **npm install**.
3. Run **ng serve**.
