import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthorizeService } from '../authorize.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  signupForm!: FormGroup;
  minDate!: Date;
  maxDate!: Date;

  constructor(private fb: FormBuilder, private authService: AuthorizeService) {}

  ngOnInit(): void {
    // Set the minimum to January 1st 100 years in the past and Dec 31st 10 years in past.
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear - 10, 11, 31);
    this.signupForm = this.fb.group({
      nickname: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      date: ['', Validators.required],
      agreement: ['', Validators.required],
    });
  }

  get nickname() {
    return this.signupForm.get('nickname');
  }

  get email() {
    return this.signupForm.get('email');
  }

  get password() {
    return this.signupForm.get('password');
  }

  get date() {
    return this.signupForm.get('date');
  }

  get agreement() {
    return this.signupForm.get('agreement');
  }

  submitSignup() {
    if (!this.signupForm.valid || !this.agreement?.value) {
      return;
    }
    console.log(JSON.stringify(this.signupForm.value));
    this.authService.signupUser({
      nickname: this.signupForm.value.nickname,
      email: this.signupForm.value.email,
      password: this.signupForm.value.password,
      dateOfBirth: this.signupForm.value.date,
      agreementDate: new Date().toDateString(),
    });
  }
}
