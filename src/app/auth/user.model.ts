export interface User {
  nickname: string;
  email: string;
  userId: string;
}
