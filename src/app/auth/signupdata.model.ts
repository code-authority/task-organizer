export interface SignupData {
  nickname: string;
  email: string;
  password: string;
  dateOfBirth: string;
  agreementDate: string;
}
