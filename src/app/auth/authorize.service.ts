import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SignupData } from './signupdata.model';
import { User } from './user.model';
import * as utils from '../utils/util-functions';
import { AuthData } from './authdata.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthorizeService {
  private user!: User;
  authStatus = new Subject<boolean>();

  constructor(private router: Router) {}

  login(authData: AuthData) {
    this.user = {
      nickname: utils.getUniqueId(1),
      email: authData.email,
      userId: utils.getUniqueId(4),
    };
    this.authStatus.next(true);
    this.router.navigate(['/dashboard']);
  }

  signupUser(signupData: SignupData) {
    this.user = {
      nickname: signupData.nickname,
      email: signupData.email,
      userId: utils.getUniqueId(4),
    };
    this.authStatus.next(true);
    this.router.navigate(['/dashboard']);
  }

  logout() {
    this.resetUser();
    this.authStatus.next(false);
    this.router.navigate(['/login']);
  }

  getUser() {
    return { ...this.user };
  }

  isAuthorized() {
    if (
      this.user != null &&
      this.user != undefined &&
      this.user.userId.length > 3
    ) {
      return true;
    } else {
      return false;
    }
  }

  private resetUser() {
    this.user = {
      nickname: '',
      email: '',
      userId: '',
    };
  }
}
