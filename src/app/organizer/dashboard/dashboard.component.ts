import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from 'src/app/auth/authorize.service';
import { User } from 'src/app/auth/user.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user!: User;

  constructor(private authService: AuthorizeService) { }

  ngOnInit(): void {
    this.user = this.authService.getUser();
  }

}
