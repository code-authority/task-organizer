import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthorizeService } from '../auth/authorize.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit, OnDestroy {
  isAuthorized: boolean = false;
  authSubscription!: Subscription;

  constructor(private authService: AuthorizeService) {}

  ngOnInit(): void {
    this.authSubscription = this.authService.authStatus.subscribe(
      (authStatus) => {
        this.isAuthorized = authStatus;
      }
    );
  }

  onLogOut() {
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }
}
